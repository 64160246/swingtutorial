package com.nannapat.swingtutorial;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class QueueApp extends JFrame {
    JTextField txtName;
    JLabel lblQueueList, lblCurrent;
    JButton btnAdd, btnGet, btnClear;
    LinkedList<String> queue;

    public QueueApp() {
        super("Queue App");
        this.setSize(400, 300);

        queue = new LinkedList();

        txtName = new JTextField();
        txtName.setBounds(30, 10, 200, 20);
        txtName.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();
            }
        });

        btnAdd = new JButton("Add Queue");
        btnAdd.setBounds(250, 10, 100, 20);
        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();
            }
        });

        btnGet = new JButton("Get Queue");
        btnGet.setBounds(250, 40, 100, 20);
        btnGet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getQueue();

            }
        });

        btnClear = new JButton("Clear Queue");
        btnClear.setBounds(250, 70, 100, 20);
        btnClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearQueue();
            }
        });

        lblQueueList = new JLabel("Empty");
        lblQueueList.setBounds(30, 40, 200, 20);

        lblCurrent = new JLabel("?");
        lblCurrent.setBounds(30, 70, 200, 50);
        lblCurrent.setHorizontalAlignment(JLabel.CENTER);
        lblCurrent.setFont(new Font("Serif", Font.PLAIN, 50));

        this.add(txtName);
        this.add(btnAdd);
        this.add(btnGet);
        this.add(btnClear);
        this.add(lblQueueList);
        this.add(lblCurrent);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        this.setVisible(true);
        showQueue();
    }

    public void showQueue() {
        if (queue.isEmpty()) {
            lblQueueList.setText("Empty");
        } else {
            lblQueueList.setText(queue.toString());
        }
    }

    public void getQueue() {
        if (queue.isEmpty()) {
            lblCurrent.setText("?");
            return;
        }
        String name = queue.remove();
        lblCurrent.setText(name);
        showQueue();
    }

    public void addQueue() {
        String name = txtName.getText();
        if (name.equals("")) {
            return;
        }
        queue.add(name);
        txtName.setText("");
        showQueue();
    }

    public void clearQueue() {
        queue.clear();
        lblCurrent.setText("?");
        txtName.setText("");
        showQueue();
    }

    public static void main(String[] args) {
        QueueApp frame = new QueueApp();
    }
}

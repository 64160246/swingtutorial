package com.nannapat.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class HelloMyName extends JFrame {
    JLabel lblName;
    JTextField textName;
    JButton btnHello;
    JLabel lblHello;

    public HelloMyName() {
        super("Hello My Name");
        lblName = new JLabel("Name: ");
        lblName.setBounds(10, 10, 100, 20);

        textName = new JTextField();
        textName.setBounds(60, 10, 200, 20);

        btnHello = new JButton("Hello");
        btnHello.setBounds(30, 40, 250, 20);
        btnHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String myName = textName.getText();
                lblHello.setText("Hello " + myName);
                ;
            }
        });

        lblHello = new JLabel("Hello My Name");
        lblHello.setHorizontalAlignment(JLabel.CENTER);
        lblHello.setBounds(30, 70, 250, 20);

        this.add(lblName);
        this.add(textName);
        this.add(btnHello);
        this.add(lblHello);
        this.setLayout(null);
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        HelloMyName frame = new HelloMyName();
    }
}
